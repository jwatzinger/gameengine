#pragma once
#include <atomic>
#include <unordered_set>
#include <mutex>

class Node
{
	using NodeContainer = std::unordered_set<Node*>;
	using LockGuard = std::lock_guard<std::mutex>;
	using MultiLockGuard = std::lock_guard<std::mutex, std::mutex>; // requires VS2015 Update 2 (C++17)
public:
	Node(void);
	~Node(void);

	void AddReference(void);
	void ReleaseReference(void);

	void SetParent(Node* pParent);
	void SetParam(int param);

	int GetResult(void);

private:

	void MarkDirty(void);
	void MarkDirtyNoLock(void);

	std::atomic<int> m_numReferences;
	int m_param, m_result;
	bool m_isDirty;

	Node* m_pParent;
	NodeContainer m_nonDirtyChilds;
	std::mutex m_paramMutex, m_parentMutex;
};

