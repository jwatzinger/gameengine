#include "Node.h"

int main(void)
{
	auto pNode1 = new Node();
	pNode1->SetParam(1);

	auto pNode2 = new Node();
	pNode2->SetParam(2);
	pNode2->SetParent(pNode1);

	auto pNode3 = new Node();
	pNode3->SetParam(3);
	pNode3->SetParent(pNode2);
	
	auto pNode4 = new Node();
	pNode4->SetParam(4);
	pNode4->SetParent(pNode3);

	const auto result4 = pNode4->GetResult(); // All nodes are calculated.
	pNode2->SetParam(20);
	int result3 = pNode3->GetResult(); // Only node2 and node3 are recalculated.
	pNode3->ReleaseReference(); // node3 wird noch von node4 benutzt.
	pNode4->ReleaseReference();
}