#include "Node.h"
#include <assert.h>

Node::Node(void) : m_numReferences(1), m_param(0), m_result(0), m_pParent(nullptr), m_isDirty(false)
{
}

Node::~Node(void)
{
	if(m_pParent)
		m_pParent->ReleaseReference();
}

void Node::AddReference(void)
{
	m_numReferences++;
}

void Node::ReleaseReference(void)
{
	if(--m_numReferences <= 0)
		delete this;
}

void Node::SetParent(Node* pParent)
{
	const LockGuard lock(m_parentMutex);

	if(pParent)
		pParent->AddReference();

	if(m_pParent)
	{
		assert(m_isDirty == (m_pParent->m_nonDirtyChilds.count(this) == 0));

		if(!m_isDirty)
			m_pParent->m_nonDirtyChilds.erase(this);
			
		m_pParent->ReleaseReference();
	}

	m_pParent = pParent;
	MarkDirtyNoLock();
}

void Node::SetParam(int param)
{
	{
		const LockGuard lock(m_paramMutex);

		m_param = param;
	}

	MarkDirty();
}

int Node::GetResult(void)
{
	// uses deadlock-avoidance
	const MultiLockGuard lock(m_paramMutex, m_parentMutex);

	if(m_isDirty)
	{
		if(m_pParent)
		{
			m_result = m_param + m_pParent->GetResult();

			assert(!m_pParent->m_nonDirtyChilds.count(this));
			m_pParent->m_nonDirtyChilds.emplace(this);
		}
		else
			m_result = m_param;

		m_isDirty = false;
	}

	return m_result;
}

void Node::MarkDirty(void)
{
	const LockGuard lock(m_parentMutex);

	MarkDirtyNoLock();
}

void Node::MarkDirtyNoLock(void)
{
	if(!m_isDirty)
	{
		if(m_pParent)
		{
			assert(m_pParent->m_nonDirtyChilds.count(this));
			m_pParent->m_nonDirtyChilds.erase(this);
		}

		const auto localDirtyChilds = m_nonDirtyChilds;
		for(auto pChild : localDirtyChilds)
		{
			pChild->MarkDirty();
		}

		m_isDirty = true;
	}
}
