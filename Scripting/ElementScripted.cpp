#include "ElementScripted.h"

std::string ElementScripted::GetValue(void) const
{
	// If the script side is still alive, then call the scripted function
	if(!m_isDead->Get())
	{
		asIScriptEngine *engine = m_obj->GetEngine();
		asIScriptContext *ctx = engine->RequestContext();

		ctx->Prepare(m_obj->GetObjectType()->GetMethodByDecl("string GetValue() const"));
		ctx->SetObject(m_obj);
		ctx->Execute();
		std::string string = *(const std::string*)ctx->GetReturnAddress();

		engine->ReturnContext(ctx);

		return string;
	}
	else
		return "";
}

// A factory function that can be used by the script side to create
ElementScripted* ElementScripted::Factory(void)
{
	auto pCtx = asGetActiveContext();
	auto pFunc = pCtx->GetFunction(0);
	if(pFunc->GetObjectType() == 0 || std::string(pFunc->GetObjectType()->GetName()) != "ElementScripted")
	{
		pCtx->SetException("Invalid attempt to manually instantiate ElementScripted_t");
		return 0;
	}

	auto pObj = reinterpret_cast<asIScriptObject*>(pCtx->GetThisPointer(0));

	auto pEngine = pCtx->GetEngine();
	auto pElement = new ElementScripted(pObj);
	asITypeInfo *type = pEngine->GetTypeInfoByName("ElementScripted_t");
	pEngine->NotifyGarbageCollectorOfNewObject(pElement, type);

	return nullptr;
}

// Reference counting
void ElementScripted::AddRef(void)
{
	m_refCount = (m_refCount & 0x7FFFFFFF) + 1;
}

void ElementScripted::Release(void)
{
	m_refCount &= 0x7FFFFFFF;
	if(--m_refCount == 0)
		delete this;
}

void ElementScripted::SetGCFlag(void)
{
	m_refCount |= 0x80000000;
}

bool ElementScripted::GetGCFlag(void)
{
	return (m_refCount & 0x80000000) ? true : false;
}

int ElementScripted::GetRefCount(void)
{
	return (m_refCount & 0x7FFFFFFF);
}

void ElementScripted::EnumReferences(asIScriptEngine *engine)
{
	engine->GCEnumCallback(m_obj);
}

void ElementScripted::ReleaseAllReferences(asIScriptEngine *engine)
{
	if(!m_isDead->Get())
	{
		m_obj->Release();
		m_obj = nullptr;
	}
}

void ElementScripted::Register(asIScriptEngine & engine)
{
	engine.RegisterObjectType("ElementScripted_t", 0, asOBJ_REF | asOBJ_GC);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_FACTORY, "ElementScripted_t @f()", asFUNCTION(ElementScripted::Factory), asCALL_CDECL);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_ADDREF, "void f()", asMETHOD(ElementScripted, AddRef), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_RELEASE, "void f()", asMETHOD(ElementScripted, Release), asCALL_THISCALL);
	engine.RegisterObjectMethod("ElementScripted_t", "string GetValue() const", asMETHOD(ElementScripted, GetValue), asCALL_THISCALL);

	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_SETGCFLAG, "void f()", asMETHOD(ElementScripted, SetGCFlag), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_GETGCFLAG, "bool f()", asMETHOD(ElementScripted, GetGCFlag), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_GETREFCOUNT, "int f()", asMETHOD(ElementScripted, GetRefCount), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_ENUMREFS, "void f(int&in)", asMETHOD(ElementScripted, EnumReferences), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("ElementScripted_t", asBEHAVE_RELEASEREFS, "void f(int&in)", asMETHOD(ElementScripted, ReleaseAllReferences), asCALL_THISCALL);
}

// The constructor and destructor are indirectly called
ElementScripted::ElementScripted(asIScriptObject *obj) : m_obj(obj), m_isDead(nullptr), m_refCount(1)
{
	m_isDead = obj->GetWeakRefFlag();
	m_isDead->AddRef();
}

ElementScripted::~ElementScripted(void)
{
	m_isDead->Release();
}