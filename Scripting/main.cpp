#include <assert.h>
#include "angelscript.h"
#include "scriptstdstring\scriptstdstring.h"
#include "scriptbuilder\scriptbuilder.h"
#include "TwiceScripted.h"

void messageCallback(const asSMessageInfo *msg, void *param)
{
	const char *type = "ERR ";
	if(msg->type == asMSGTYPE_WARNING)
		type = "WARN";
	else if(msg->type == asMSGTYPE_INFORMATION)
		type = "INFO";
	printf("%s (%d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message);
}

void print(const std::string& msg)
{
	printf("%s", msg.c_str());
}

int main(void)
{
	// setup engine
	auto pEngine = asCreateScriptEngine();
	pEngine->SetMessageCallback(asFUNCTION(messageCallback), 0, asCALL_CDECL);

	// register print-function
	RegisterStdString(pEngine);
	pEngine->RegisterGlobalFunction("void print(const string &in)", asFUNCTION(print), asCALL_CDECL);

	ElementScripted::Register(*pEngine);
	TwiceScripted::Register(*pEngine);

	// load custom script
	CScriptBuilder builder;
	builder.StartNewModule(pEngine, "MyModule");
	builder.AddSectionFromFile("script.as");
	builder.BuildModule();

	// execute script function
	auto pModule = pEngine->GetModule("MyModule");
	auto pFunc = pModule->GetFunctionByDecl("void main()");
	assert(pFunc);

	// Create our context, prepare it, and then execute
	auto pCtx = pEngine->CreateContext();
	pCtx->Prepare(pFunc);
	const auto r = pCtx->Execute();
	assert(r == asEXECUTION_FINISHED);

	pEngine->GarbageCollect(asGC_FULL_CYCLE);

	pCtx->Release();
	pEngine->ShutDownAndRelease();

	return 0;
}