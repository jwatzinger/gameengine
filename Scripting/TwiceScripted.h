#pragma once
#include "Twice.h"
#include "ElementScripted.h"

class TwiceScripted :
	public Twice
{
public:

	static TwiceScripted* Factory(ElementScripted* pParent);

	void AddRef(void);
	void Release(void);

	void SetGCFlag(void);
	bool GetGCFlag(void);
	int GetRefCount(void);
	void EnumReferences(asIScriptEngine * engine);
	void ReleaseAllReferences(asIScriptEngine * engine);

	static void Register(asIScriptEngine& engine);

protected:
	TwiceScripted(asIScriptObject* pObj, ElementScripted* pParent);
	~TwiceScripted(void);

	int m_refCount;

	asILockableSharedBool* m_pIsDead;
	asIScriptObject *m_pObj;
	ElementScripted* m_pParent;
};
