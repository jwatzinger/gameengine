#include "Twice.h"

Twice::Twice(Element* pParent) : m_pParent(pParent)
{
}

std::string Twice::GetValue(void) const
{
	return m_pParent->GetValue() + " " + m_pParent->GetValue();
}