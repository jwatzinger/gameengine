#pragma once
#include "Element.h"
#include <string>
#include "angelscript.h"

class ElementScripted :
	public Element
{
public:
	std::string GetValue(void) const override;

	static ElementScripted* Factory(void);

	void AddRef(void);
	void Release(void);

	void SetGCFlag(void);
	bool GetGCFlag(void);
	int GetRefCount(void);
	void EnumReferences(asIScriptEngine* engine);
	void ReleaseAllReferences(asIScriptEngine* engine);

	static void Register(asIScriptEngine& engine);

protected:
	ElementScripted(asIScriptObject *obj);
	~ElementScripted(void);

	int m_refCount;

	asILockableSharedBool *m_isDead;
	asIScriptObject *m_obj;
};
