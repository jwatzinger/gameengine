#pragma once
#include "Element.h"

class Twice :
	public Element
{
public:
	Twice(Element* pParent);
	
	std::string GetValue(void) const override final;

private:

	Element* m_pParent;
};

