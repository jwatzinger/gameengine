#include "TwiceScripted.h"

// A factory function that can be used by the script side to create
TwiceScripted* TwiceScripted::Factory(ElementScripted* pParent)
{
	auto pCtx = asGetActiveContext();
	auto pFunc = pCtx->GetFunction(0);
	if(pFunc->GetObjectType() == 0 || std::string(pFunc->GetObjectType()->GetName()) != "TwiceScripted")
	{
		pCtx->SetException("Invalid attempt to manually instantiate ElementScripted_t");
		return 0;
	}

	auto pObj = reinterpret_cast<asIScriptObject*>(pCtx->GetThisPointer(0));

	auto pEngine = pCtx->GetEngine();
	auto pTwice = new TwiceScripted(pObj, pParent);
	asITypeInfo *type = pEngine->GetTypeInfoByName("TwiceScripted_t");
	pEngine->NotifyGarbageCollectorOfNewObject(pTwice, type);
}

// Reference counting
void TwiceScripted::AddRef(void)
{
	m_refCount = (m_refCount & 0x7FFFFFFF) + 1;
}

void TwiceScripted::Release(void)
{
	m_refCount &= 0x7FFFFFFF;
	if(--m_refCount == 0)
		delete this;
}

void TwiceScripted::SetGCFlag(void)
{
	m_refCount |= 0x80000000;
}

bool TwiceScripted::GetGCFlag(void)
{
	return (m_refCount & 0x80000000) ? true : false;
}

int TwiceScripted::GetRefCount(void)
{
	return (m_refCount & 0x7FFFFFFF);
}

void TwiceScripted::EnumReferences(asIScriptEngine* engine)
{
	engine->GCEnumCallback(m_pObj);
	engine->GCEnumCallback(m_pParent);
}

void TwiceScripted::ReleaseAllReferences(asIScriptEngine* engine)
{
	if(m_pObj)
	{
		m_pObj->Release();
		m_pObj = nullptr;
	}

	if(m_pParent)
	{
		m_pParent->Release();
		m_pParent = nullptr;
	}
}

void TwiceScripted::Register(asIScriptEngine & engine)
{
	engine.RegisterObjectType("TwiceScripted_t", 0, asOBJ_REF | asOBJ_GC);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_FACTORY, "TwiceScripted_t @f(ElementScripted_t@)", asFUNCTION(TwiceScripted::Factory), asCALL_CDECL);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_ADDREF, "void f()", asMETHOD(TwiceScripted, AddRef), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_RELEASE, "void f()", asMETHOD(TwiceScripted, Release), asCALL_THISCALL);
	engine.RegisterObjectMethod("TwiceScripted_t", "string GetValue() const", asMETHOD(TwiceScripted, GetValue), asCALL_THISCALL);

	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_SETGCFLAG, "void f()", asMETHOD(TwiceScripted, SetGCFlag), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_GETGCFLAG, "bool f()", asMETHOD(TwiceScripted, GetGCFlag), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_GETREFCOUNT, "int f()", asMETHOD(TwiceScripted, GetRefCount), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_ENUMREFS, "void f(int&in)", asMETHOD(TwiceScripted, EnumReferences), asCALL_THISCALL);
	engine.RegisterObjectBehaviour("TwiceScripted_t", asBEHAVE_RELEASEREFS, "void f(int&in)", asMETHOD(TwiceScripted, ReleaseAllReferences), asCALL_THISCALL);
}

// The constructor and destructor are indirectly called
TwiceScripted::TwiceScripted(asIScriptObject* pObj, ElementScripted* pParent) : 
	Twice(pParent), m_pObj(pObj), m_pIsDead(nullptr), m_refCount(1), m_pParent(pParent)
{
	m_pIsDead = pObj->GetWeakRefFlag();
	m_pIsDead->AddRef();

	m_pParent->AddRef();
}

TwiceScripted::~TwiceScripted(void)
{
	m_pIsDead->Release();
	if(m_pParent)
		m_pParent->Release();
}