shared class ElementScripted
{
	ElementScripted()
	{
		@m_obj = ElementScripted_t(); 
	}

	string GetValue() const
	{ 
		return m_obj.GetValue(); 
	}

	ElementScripted_t @opImplCast() 
	{ 
		return m_obj; 
	}

	private ElementScripted_t @m_obj;
}

shared class TwiceScripted
{
	TwiceScripted(ElementScripted@ parent)
	{
		@m_obj = TwiceScripted_t(parent); 
	}

	string GetValue() const
	{ 
		return m_obj.GetValue(); 
	}

	TwiceScripted_t @opImplCast() 
	{ 
		return m_obj; 
	}

	private TwiceScripted_t @m_obj;
}

class MyParent : ElementScripted
{
	string GetValue() const override
	{
		return "hello world";
	}
	
	TwiceScripted @m_twice;
}

void main()
{
	MyParent@ parent = MyParent();
	TwiceScripted@ twice = TwiceScripted(parent);
	
	print(twice.GetValue());
	
	@parent.m_twice = twice;
}