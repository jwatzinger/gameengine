#pragma once
#include <string>

class Element
{
public:

	virtual std::string GetValue(void) const = 0;
};